package course.example.myandroidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    Button button;
    Button button2;
    Button button3;
    Button button4;
    ImageView image;
    Intent intent;
    public final static String EXTRA_MESSAGE = "course.example.myandroidapp.MESSAGE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {
        intent = new Intent(this,Main2Activity.class);
        image = (ImageView) findViewById(R.id.imageView1);

        button = (Button) findViewById(R.id.btnChangeImage);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "1");
                startActivity(intent);
            }
        });

        button2 = (Button) findViewById(R.id.btnChangeImage2);
        button2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "2");
                startActivity(intent);
            }
        });

        button3 = (Button) findViewById(R.id.btnChangeImage3);
        button3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "3");
                startActivity(intent);
            }
        });

        button4 = (Button) findViewById(R.id.btnChangeImage4);
        button4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                intent.putExtra(EXTRA_MESSAGE, "4");
                startActivity(intent);
            }
        });
    }
}
